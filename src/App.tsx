import MagnetCursor from "@components/Magnet/MagnetCursor"
import MagnetBlock from "@components/Magnet/MagnetBlock"

function App() {
    return (
        <>
            <div style={{ display: "flex", marginTop: 200, marginLeft: 200 }}>
                <MagnetBlock>MEOW</MagnetBlock>
            </div>

            <div style={{ display: "flex" }}>
                <MagnetBlock>
                    <div
                        style={{
                            width: 100,
                            height: 100,
                            backgroundColor: "green",
                        }}
                    ></div>
                </MagnetBlock>
            </div>
            <MagnetCursor />
        </>
    )
}

export default App
