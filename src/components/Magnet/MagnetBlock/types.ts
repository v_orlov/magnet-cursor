import { ReactNode } from "react"

export interface IMagnetBlockProps {
    children: ReactNode
}

export interface IMagnetBlock {
    areaX: number
    areaY: number
    width: number
    height: number
    ref: HTMLDivElement
}
