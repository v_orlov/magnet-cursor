import {useActions} from "@redux/reducers/hooks/actions"
import React, {useEffect, useRef} from "react"

import {IMagnetBlockProps,} from "@components/Magnet/MagnetBlock/types"

const MagnetBlock: React.FC<IMagnetBlockProps> = ({ children }) => {
    const magnetBlockRef = useRef<HTMLDivElement>(null)

    const { addMagnet, removeMagnet } = useActions()

    useEffect(() => {
        if (!magnetBlockRef.current) {
            return
        }

        const magnetElement = magnetBlockRef.current

        addMagnet(magnetElement)

        return () => {
            removeMagnet(magnetElement)
        }
    }, [magnetBlockRef])

    useEffect(() => {

    }, []);

    return (
        <div ref={magnetBlockRef} style={{ marginTop: 30 }}>
            {children}
        </div>
    )
}

export default MagnetBlock
