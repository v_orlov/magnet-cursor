import { spreadArea } from "./constants"

export const initMagnetBlock = (element: HTMLDivElement) => {
    const { width, height, x, y } = element.getBoundingClientRect()

    return {
        areaX: width + x + spreadArea,
        areaY: height + y + spreadArea,
        y,
        x,
        width,
        height,
        ref: element,
    }
}
