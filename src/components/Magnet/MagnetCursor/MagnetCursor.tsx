import {useEffect, useState} from "react"
import {useSelector} from "react-redux"
import {animated, useSpring} from "@react-spring/web"

import {useActions} from "@redux/reducers/hooks/actions"

import {IMagnetBlock} from "@components/Magnet/MagnetBlock/types"

import {IMagnetPosition} from "./types"
import {store} from "@redux/store.ts";

const MagnetCursor = () => {
    const [position, setPosition] = useState<IMagnetPosition>({
        x: 0,
        y: 0,
        width: 30,
        height: 30,
    })

    const { resize } = useActions()

    const [style, animate] = useSpring(
        () => ({ left: "0px", right: "0px" }),
        []
    )
    const [styleHeight, animateHeight] = useSpring(
        () => ({ height: "30px", width: "30px" }),
        []
    )

    const [isAttracted, setIsAttracted] = useState<boolean>(false)

    useEffect(() => {
        animate({
            top: position.y + "px",
            left: position.x + "px",
            config: {
                duration: isAttracted ? 200 : 0,
            },
        })
    }, [position, isAttracted])

    useEffect(() => {
        animateHeight({
            width: position.width + "px",
            height: position.height + "px",
            config: {
                duration: 200,
                friction: 10,
            },
        })
    }, [position])

    useEffect(() => {
        addEventListener("mousemove", handleMouseMove)
        addEventListener("resize", handleResize)
        return () => {
            removeEventListener("mousemove",handleMouseMove)
            removeEventListener("resize", handleResize)
        }
    }, [])

    const handleResize = () => {
        resize()
    }

    const handleMouseMove = ({ x, y }: MouseEvent) => {
        const attractedMagnet = store.getState().magnetSlice.magnets?.find((magnet: IMagnetBlock) => {
            if (
                x >= magnet.x - 50 &&
                x <= magnet.areaX &&
                y >= magnet.y - 50 &&
                y <= magnet.areaY
            ) {
                return true
            }
        })

        if (attractedMagnet) {
            setIsAttracted(true)

            setPosition({
                y: attractedMagnet.y,
                x: attractedMagnet.x,
                width: attractedMagnet.width,
                height: attractedMagnet.height,
            })

            return
        }

        setIsAttracted(false)

        setPosition({
            y: y - 10,
            x: x - 10,
            width: 30,
            height: 30,
        })
    }


    return (
        <div>
            <animated.div
                className="MagnetCursor"
                style={{ ...style, ...styleHeight }}
            >
                <div className="MagnetCursor__Corner" />
                <div className="MagnetCursor__Corner" />
                <div className="MagnetCursor__Corner" />
                <div className="MagnetCursor__Corner" />
                <div className="MagnetCursor__Corner" />
            </animated.div>
        </div>
    )
}

export default MagnetCursor
