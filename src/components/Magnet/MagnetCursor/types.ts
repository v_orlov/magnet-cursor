export interface IMagnetPosition {
    y: number
    x: number
    width: number
    height: number
}
