import { configureStore } from "@reduxjs/toolkit"
import rootReducer from "./reducers"
import { setupListeners } from "@reduxjs/toolkit/query"

export const store = configureStore({
    reducer: {
        ...rootReducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({ serializableCheck: false }),
})

setupListeners(store.dispatch)

export type StateType = ReturnType<typeof store.getState>
