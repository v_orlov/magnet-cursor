import {createSlice, PayloadAction} from "@reduxjs/toolkit"
import {initMagnetBlock} from "@components/Magnet/utils"

import {IMagnetBlock} from "@components/Magnet/MagnetBlock/types";

interface IMagnetState {
    magnets: IMagnetBlock[]
}

const initialState: IMagnetState = {
    magnets: [],
}

const magnetSlice = createSlice({
    name: "magnet",
    initialState,
    reducers: {
        addMagnet: (state, action: PayloadAction<HTMLDivElement>) => {
            state.magnets.push(initMagnetBlock(action.payload))
        },
        removeMagnet: (state, action: PayloadAction<HTMLDivElement>) => {
            state.magnets = state.magnets
                .filter(({ ref }) => ref !== action.payload)
                .map((magnet) => initMagnetBlock(magnet.ref))
        },
        resize: (state) => {
            state.magnets = state.magnets.map((magnet) =>
                initMagnetBlock(magnet.ref)
            )
        },
    },
})

export const magnetSliceActions = magnetSlice.actions
export default magnetSlice.reducer
