import { magnetSliceActions } from "../magnet"
import { useDispatch } from "react-redux"
import { bindActionCreators } from "@reduxjs/toolkit"

const actions = {
    ...magnetSliceActions,
}

export const useActions = () => {
    const dispatch = useDispatch()
    return bindActionCreators(actions, dispatch)
}
