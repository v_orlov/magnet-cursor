import { defineConfig } from "vite"
import react from "@vitejs/plugin-react"
import sass from "sass"
import { resolve } from "path"

export default defineConfig({
    plugins: [react()],
    resolve: {
        alias: [
            {
                find: "@redux",
                replacement: resolve(__dirname, "./src/redux"),
            },
            {
                find: "@components",
                replacement: resolve(__dirname, "./src/components"),
            },
        ],
    },
    css: {
        preprocessorOptions: {
            scss: {
                implementation: sass,
            },
        },
    },
})
